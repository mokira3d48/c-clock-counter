# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>
# include <sys/types.h>
# include <time.h>
# include <string.h>
# include <signal.h>

# include "cpt_clock.h"

void stop_clock ( pid_t pid ) {

	
	char c;

	do {
		c = getchar ();

		switch ( c ) {

			case '0' :
				kill ( pid, SIGKILL );
				break;

			default : break;
		}
	
	}
	while ( c != '0' );

	printf ( " The time is stoped ! ;)\n" );

}

void showinloopcptclock () {
	//
	time_t     seconde;
	struct tm  inst;
	char       date_formated [128];

	while ( 1 ) {
		system ( "clear" );
		time   ( &seconde );

		inst = ( *( localtime ( &seconde ) ) );

		strftime ( date_formated, 128, "\n \t%A \t %x\n \t%I : %M : %S \t  %p\n \t%Z\n", &inst );

		puts   ( date_formated );
		sleep  ( 1 );
	}
}
