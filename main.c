# include <stdlib.h>
# include <unistd.h>
# include <stdio.h>
# include <sys/types.h>
# include <errno.h>

# include "cpt_clock.h"


int main ( void ) {

	pid_t pid;


	do {
		pid = fork ();

	}
	while ( pid == -1 && errno == EAGAIN );

	switch ( pid ) {
		case -1 :
			printf ( " [ ERROR ] :: error code : %d\n", errno );
			break;

		case 0 :
			showinloopcptclock ();
			break;

		default :
			stop_clock ( pid );
			break;

	}

	return 0;
}
